package com.example.demo.app.team;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.app.team.data.TeamDTO;
import com.example.demo.app.team.data.TeamModificationRequest;
import com.example.demo.app.team.service.TeamService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;

@RestController
@Api(tags = "Team API", value = "team")
@RequestMapping(value = "/team")
@RequiredArgsConstructor
public class TeamController {
	private final TeamService teamService;

	@ApiOperation(value = "Creates new team")
	@PostMapping(value = "/create",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public TeamDTO createTeam(
			@ApiParam(name = "team", value = "Team metadata needed to create entity", required = true)
			@RequestBody TeamModificationRequest team) {
		return teamService.createTeam(team);
	}

	@ApiOperation(value = "Updates team entity")
	@PutMapping(value = "/update",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public TeamDTO updateTeam(
			@ApiParam(name = "team", value = "Team metadata needed to update entity", required = true)
			@RequestBody TeamModificationRequest team) {
		return teamService.updateTeam(team);
	}

	@ApiOperation(value = "Deletes team")
	@DeleteMapping(value = "/delete",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public void deleteTeam(
			@ApiParam(name = "teamId", value = "Team id which is going to be deleted", required = true)
			@RequestParam(name = "teamId") long teamId) {
		teamService.deleteTeam(teamId);
	}

	@ApiOperation(value = "Gets team for player")
	@GetMapping(value = "/get",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public TeamDTO getTeam(
			@ApiParam(name = "playerId", value = "Player id whose team will be fetched", required = true)
			@RequestParam(name = "playerId") long playerId) {
		return teamService.getTeam(playerId);
	}
}

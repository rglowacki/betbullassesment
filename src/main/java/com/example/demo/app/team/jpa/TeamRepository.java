package com.example.demo.app.team.jpa;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TeamRepository extends JpaRepository<Team, Long> {
	Optional<Team> findByTeamId(Long teamId);

	@Query(value = "SELECT t.* FROM team t JOIN player p ON t.team_id = p.team_id " +
			"WHERE p.player_id = :playerId",
			nativeQuery = true)
	Optional<Team> findTeamByPlayerId(@Param("playerId") long playerId);
}

package com.example.demo.app.team.data;

import java.util.Objects;

import com.example.demo.app.team.jpa.Team;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class TeamDTO {

	@ApiModelProperty(notes = "Team id")
	long teamId;
	@ApiModelProperty(notes = "Team name")
	String teamName;
	@ApiModelProperty(notes = "Currency used by team")
	String teamCurrency;

	public static TeamDTO toTeamDTO(Team team) {
		return Objects.nonNull(team) ? TeamDTO.builder()
				.teamId(team.getTeamId())
				.teamName(team.getTeamName())
				.teamCurrency(team.getTeamCurrency()).build() : null;
	}
}

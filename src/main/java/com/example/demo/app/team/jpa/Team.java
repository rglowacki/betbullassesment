package com.example.demo.app.team.jpa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.example.demo.app.player.jpa.Player;
import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "TEAM")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Team {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEAM_GENERATOR")
	@SequenceGenerator(
			name = "TEAM_GENERATOR",
			sequenceName = "TEAM_SEQUENCE",
			allocationSize = 1
	)
	@Column(name = "TEAM_ID")
	private Long teamId;

	@Column(name = "TEAM_NAME")
	private String teamName;

	@Column(name = "TEAM_CURRENCY")
	private String teamCurrency;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "team")
	@JsonBackReference
	private List<Player> teamPlayers = new ArrayList<>();
}

package com.example.demo.app.team.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class TeamModificationRequest {
	@ApiModelProperty(notes = "Team id")
	long teamId;
	@ApiModelProperty(notes = "Team name")
	String teamName;
	@ApiModelProperty(notes = "Currency used by team")
	String teamCurrency;
}

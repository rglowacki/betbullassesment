package com.example.demo.app.team.service;

import static com.example.demo.app.team.data.TeamDTO.toTeamDTO;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.stereotype.Service;

import com.example.demo.app.player.jpa.Player;
import com.example.demo.app.player.jpa.PlayerRepository;
import com.example.demo.app.team.data.TeamDTO;
import com.example.demo.app.team.data.TeamModificationRequest;
import com.example.demo.app.team.jpa.Team;
import com.example.demo.app.team.jpa.TeamRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class TeamService {
	private final TeamRepository teamRepository;
	private final PlayerRepository playerRepository;

	public TeamDTO createTeam(TeamModificationRequest team) {
		Team newTeam = Team.builder()
				.teamName(team.getTeamName())
				.teamCurrency(team.getTeamCurrency()).build();
		return toTeamDTO(teamRepository.save(newTeam));
	}

	public TeamDTO updateTeam(TeamModificationRequest team) {
		Team teamToUpdate = teamRepository.findByTeamId(team.getTeamId()).orElseThrow(
				() -> new EntityNotFoundException(String.format("Team with id %d not found", team.getTeamId()))
		);
		teamToUpdate.setTeamName(team.getTeamName());
		teamToUpdate.setTeamCurrency(team.getTeamCurrency());
		return toTeamDTO(teamRepository.save(teamToUpdate));
	}

	public void deleteTeam(long teamId) {
		Team teamToDelete = teamRepository.findByTeamId(teamId).orElseThrow(
				() -> new EntityNotFoundException(String.format("Team with id %d not found", teamId))
		);
		List<Player> playersAssignedToTeam = playerRepository.findAllByTeam_TeamId(teamToDelete.getTeamId());
		playersAssignedToTeam.forEach(player -> player.setTeam(null));
		playerRepository.saveAll(playersAssignedToTeam);
		teamRepository.delete(teamToDelete);
	}

	public TeamDTO getTeam(long playerId) {
		Optional<Team> team = teamRepository.findTeamByPlayerId(playerId);
		return team.isPresent() ? toTeamDTO(team.get()) : TeamDTO.builder().build();
	}
}

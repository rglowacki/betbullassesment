package com.example.demo.app.player.service;


import static com.example.demo.app.player.data.PlayerDTO.toPlayerDTO;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.springframework.stereotype.Service;

import com.example.demo.app.player.data.PlayerModificationRequest;
import com.example.demo.app.player.data.PlayerDTO;
import com.example.demo.app.player.jpa.Player;
import com.example.demo.app.player.jpa.PlayerRepository;
import com.example.demo.app.team.jpa.Team;
import com.example.demo.app.team.jpa.TeamRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class PlayerService {

	private final PlayerRepository playerRepository;
	private final TeamRepository teamRepository;

	public PlayerDTO createPlayer(PlayerModificationRequest player) {
		Optional<Team> team = teamRepository.findByTeamId(player.getTeamId());
		Player newPlayer = Player.builder()
				.playerName(player.getPlayerName())
				.playerAge(player.getPlayerAge())
				.team(team.orElse(null))
				.monthsOfExperience(player.getMonthsOfExperience()).build();
		return toPlayerDTO(playerRepository.save(newPlayer));
	}

	public PlayerDTO updatePlayer(PlayerModificationRequest player) {
		Player playerToUpdate = playerRepository.findByPlayerId(player.getPlayerId()).orElseThrow(
				() -> new EntityNotFoundException(String.format("Player with id %d not found", player.getPlayerId()))
		);
		Optional<Team> team = teamRepository.findByTeamId(player.getTeamId());
		playerToUpdate.setPlayerName(player.getPlayerName());
		playerToUpdate.setPlayerAge(player.getPlayerAge());
		playerToUpdate.setMonthsOfExperience(player.getMonthsOfExperience());
		team.ifPresent(playerToUpdate::setTeam);
		return toPlayerDTO(playerRepository.save(playerToUpdate));
	}

	public void deletePlayer(long playerId) {
		Player playerToDelete = playerRepository.findByPlayerId(playerId).orElseThrow(
				() -> new EntityNotFoundException(String.format("Player with id %d not found", playerId))
		);
		playerRepository.delete(playerToDelete);
	}

	public BigDecimal getPlayerTransferFee(long playerId) {
		Player playerToTransfer = playerRepository.findByPlayerId(playerId).orElseThrow(
				() -> new EntityNotFoundException(String.format("Player with id %d not found", playerId))
		);
		BigDecimal playerAge = new BigDecimal(playerToTransfer.getPlayerAge());
		BigDecimal playerBasePrice = new BigDecimal(100000);
		BigDecimal monthsOfExperienceMultiplier = new BigDecimal(playerToTransfer.getMonthsOfExperience());
		BigDecimal transferFee = monthsOfExperienceMultiplier.multiply(playerBasePrice).divide(playerAge, 2, BigDecimal.ROUND_HALF_DOWN);
		BigDecimal teamCommission = transferFee.divide(BigDecimal.TEN, 2, BigDecimal.ROUND_HALF_DOWN);
		return teamCommission.add(transferFee);
	}

	public List<PlayerDTO> getPlayers() {
		return playerRepository.findAll().stream()
				.map(PlayerDTO::toPlayerDTOLazy)
				.collect(Collectors.toList());
	}
}

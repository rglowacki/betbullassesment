package com.example.demo.app.player.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class PlayerModificationRequest {
	@ApiModelProperty(notes = "Player id")
	long playerId;
	@ApiModelProperty(notes = "Player name")
	String playerName;
	@ApiModelProperty(notes = "Player age")
	int playerAge;
	@ApiModelProperty(notes = "Player team id")
	Long teamId;
	@ApiModelProperty(notes = "Player experience in months")
	int monthsOfExperience;
}

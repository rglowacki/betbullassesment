package com.example.demo.app.player.jpa;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerRepository extends JpaRepository<Player, Long> {

	Optional<Player> findByPlayerId(long playerId);

	List<Player> findAllByTeam_TeamId(long teamId);
}

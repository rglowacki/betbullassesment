package com.example.demo.app.player;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.app.player.data.PlayerDTO;
import com.example.demo.app.player.data.PlayerModificationRequest;
import com.example.demo.app.player.service.PlayerService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;

@RestController
@Api(tags = "Player API", value = "player")
@RequestMapping(value = "/player")
@RequiredArgsConstructor
public class PlayerController {

	private final PlayerService playerService;

	@ApiOperation(value = "Creates new player")
	@PostMapping(value = "/create",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public PlayerDTO createPlayer(
			@ApiParam(name = "player", value = "Player metadata needed to create  entity", required = true)
			@RequestBody PlayerModificationRequest player) {
		return playerService.createPlayer(player);
	}

	@ApiOperation(value = "Updates player entity")
	@PutMapping(value = "/update",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public PlayerDTO updatePlayer(
			@ApiParam(name = "player", value = "Player metadata needed to update  entity", required = true)
			@RequestBody PlayerModificationRequest player) {
		return playerService.updatePlayer(player);
	}

	@ApiOperation(value = "Deletes player")
	@DeleteMapping(value = "/delete",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public void deletePlayer(
			@ApiParam(name = "playerId", value = "Player id which is going to be deleted", required = true)
			@RequestParam(name = "playerId") long playerId) {
		playerService.deletePlayer(playerId);
	}

	@ApiOperation(value = "Finds all players")
	@GetMapping(value = "/find-all",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PlayerDTO> getPlayers() {
		return playerService.getPlayers();
	}

	@ApiOperation(value = "Gets player contract fee")
	@GetMapping(value = "/transfer-fee",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public BigDecimal getPlayerFee(
			@ApiParam(name = "playerId", value = "Player id which is going to be transferred", required = true)
			@RequestParam(name = "playerId") long playerId) {
		return playerService.getPlayerTransferFee(playerId);
	}




}

package com.example.demo.app.player.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.example.demo.app.team.jpa.Team;
import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "PLAYER")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Player {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PLAYER_GENERATOR")
	@SequenceGenerator(
			name = "PLAYER_GENERATOR",
			sequenceName = "PLAYER_SEQUENCE",
			allocationSize = 1
	)
	@Column(name = "PLAYER_ID")
	private Long playerId;

	@Column(name = "PLAYER_NAME")
	private String playerName;

	@Column(name = "PLAYER_AGE")
	private int playerAge;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TEAM_ID")
	@JsonBackReference
	private Team team;

	@Column(name = "PLAYER_EXPERIENCE")
	private int monthsOfExperience;
}

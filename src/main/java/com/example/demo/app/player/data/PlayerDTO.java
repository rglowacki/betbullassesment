package com.example.demo.app.player.data;

import static com.example.demo.app.team.data.TeamDTO.toTeamDTO;

import com.example.demo.app.player.jpa.Player;
import com.example.demo.app.team.data.TeamDTO;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class PlayerDTO {
	@ApiModelProperty(notes = "Player id")
	long playerId;
	@ApiModelProperty(notes = "Player name")
	String playerName;
	@ApiModelProperty(notes = "Player age")
	int playerAge;
	@ApiModelProperty(notes = "Player team")
	TeamDTO team;
	@ApiModelProperty(notes = "Player experience in months")
	int monthsOfExperience;

	public static PlayerDTO toPlayerDTO(Player player) {
		return PlayerDTO.builder()
				.playerId(player.getPlayerId())
				.playerName(player.getPlayerName())
				.playerAge(player.getPlayerAge())
				.team(toTeamDTO(player.getTeam()))
				.monthsOfExperience(player.getMonthsOfExperience())
				.build();
	}

	public static PlayerDTO toPlayerDTOLazy(Player player) {
		return PlayerDTO.builder()
				.playerId(player.getPlayerId())
				.playerName(player.getPlayerName())
				.playerAge(player.getPlayerAge())
				.monthsOfExperience(player.getMonthsOfExperience())
				.build();
	}

}

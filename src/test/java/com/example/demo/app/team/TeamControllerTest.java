package com.example.demo.app.team;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.app.team.data.TeamModificationRequest;
import com.example.demo.app.team.service.TeamService;

@RunWith(SpringRunner.class)
public class TeamControllerTest {

	@MockBean
	private TeamService teamService;

	private TeamController toTest;
	private TeamModificationRequest request;

	@Before
	public void setUp() {
		toTest = new TeamController(teamService);
		request = TeamModificationRequest.builder()
				.teamId(1L)
				.teamCurrency("USD")
				.teamName("Raf FC").build();
	}

	@Test
	public void should_createTeam_call_service_method_with_proper_arguments() {
		//when
		toTest.createTeam(request);
		//then
		BDDMockito.then(teamService).should().createTeam(request);
	}

	@Test
	public void should_updateTeam_call_service_method_with_proper_arguments() {
		//when
		toTest.updateTeam(request);
		//then
		BDDMockito.then(teamService).should().updateTeam(request);
	}

	@Test
	public void should_deleteTeam_call_service_method_with_proper_arguments() {
		//when
		toTest.deleteTeam(request.getTeamId());
		//then
		BDDMockito.then(teamService).should().deleteTeam(request.getTeamId());
	}

	@Test
	public void should_getTeam_call_service_method_with_proper_arguments() {
		//given
		long playerId = 2L;
		//when
		toTest.getTeam(playerId);
		//then
		BDDMockito.then(teamService).should().getTeam(playerId);
	}
}

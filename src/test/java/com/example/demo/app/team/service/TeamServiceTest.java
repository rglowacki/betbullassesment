package com.example.demo.app.team.service;

import static com.example.demo.app.team.data.TeamDTO.toTeamDTO;
import static org.assertj.core.api.BDDAssertions.then;
import static org.assertj.core.api.BDDAssertions.thenThrownBy;
import static org.mockito.BDDMockito.given;

import java.util.Arrays;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.assertj.core.api.BDDAssertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.app.player.jpa.Player;
import com.example.demo.app.player.jpa.PlayerRepository;
import com.example.demo.app.team.data.TeamDTO;
import com.example.demo.app.team.data.TeamModificationRequest;
import com.example.demo.app.team.jpa.Team;
import com.example.demo.app.team.jpa.TeamRepository;

@RunWith(SpringRunner.class)
public class TeamServiceTest {

	@MockBean
	private TeamRepository teamRepository;
	@MockBean
	private PlayerRepository playerRepository;

	private TeamService toTest;
	private TeamModificationRequest request;
	private Team team;

	@Before
	public void setUp() {
		toTest = new TeamService(teamRepository, playerRepository);
		request = TeamModificationRequest.builder()
				.teamId(1L)
				.teamCurrency("USD")
				.teamName("Raf FC").build();
		team = Team.builder()
				.teamId(1L)
				.teamCurrency("USD")
				.teamName("Raf FC").build();
	}

	@Test
	public void should_createTeam_success() {
		//given
		Team savedTeam = Team.builder()
				.teamName(request.getTeamName())
				.teamCurrency(request.getTeamCurrency()).build();
		given(teamRepository.save(savedTeam)).willReturn(team);
		//when
		TeamDTO result = toTest.createTeam(request);
		//then
		then(result.getTeamCurrency()).isEqualTo(request.getTeamCurrency());
		then(result.getTeamName()).isEqualTo(request.getTeamName());
	}

	@Test
	public void should_updatedTeam_throw_exception_when_team_not_found() {
		//when
		thenThrownBy(() -> toTest.updateTeam(request))
				//then
				.isInstanceOf(EntityNotFoundException.class)
				.hasMessage(String.format("Team with id %d not found", request.getTeamId()));
	}

	@Test
	public void should_updateTeam_success() {
		//given
		TeamModificationRequest updateRequest = TeamModificationRequest.builder()
				.teamId(1L)
				.teamName("FC Raf")
				.teamCurrency("EUR").build();
		Team updatedTeam = Team.builder()
				.teamId(updateRequest.getTeamId())
				.teamName(updateRequest.getTeamName())
				.teamCurrency(updateRequest.getTeamCurrency()).build();
		given(teamRepository.findByTeamId(updateRequest.getTeamId())).willReturn(Optional.of(team));
		given(teamRepository.save(updatedTeam)).willReturn(updatedTeam);
		//when
		TeamDTO result = toTest.updateTeam(updateRequest);
		//then
		then(result.getTeamCurrency()).isEqualTo(updateRequest.getTeamCurrency());
		then(result.getTeamName()).isEqualTo(updateRequest.getTeamName());
		then(result.getTeamId()).isEqualTo(updateRequest.getTeamId());
	}

	@Test
	public void should_deleteTeam_throw_exception_when_team_not_found() {
		//when
		thenThrownBy(() -> toTest.updateTeam(request))
				//then
				.isInstanceOf(EntityNotFoundException.class)
				.hasMessage(String.format("Team with id %d not found", request.getTeamId()));
	}

	@Test
	public void should_deleteTeam_success_and_remove_team_from_assigned_players() {
		//given
		Player firstPlayer = Player.builder().team(team).build();
		Player secondPlayer = Player.builder().team(team).build();
		given(teamRepository.findByTeamId(request.getTeamId())).willReturn(Optional.of(team));
		given(playerRepository.findAllByTeam_TeamId(request.getTeamId()))
				.willReturn(Arrays.asList(firstPlayer, secondPlayer));
		//when
		BDDAssertions.assertThatCode(() -> toTest.deleteTeam(request.getTeamId())).doesNotThrowAnyException();
		//then
		then(firstPlayer.getTeam()).isNull();
		then(secondPlayer.getTeam()).isNull();
		BDDMockito.then(teamRepository).should().delete(team);
	}

	@Test
	public void should_getTeam_return_empty_dto_when_team_not_found() {
		//given
		long playerId = 1L;
		//when
		TeamDTO result = toTest.getTeam(playerId);
		//then
		then(result).isEqualTo(TeamDTO.builder().build());
	}

	@Test
	public void should_getTeam_return_players_team() {
		//given
		long playerId = 1L;
		given(teamRepository.findTeamByPlayerId(playerId)).willReturn(Optional.of(team));
		//when
		TeamDTO result = toTest.getTeam(playerId);
		//then
		then(result).isEqualTo(toTeamDTO(team));
	}
}

package com.example.demo.app.player;

import static org.mockito.ArgumentMatchers.argThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.app.player.data.PlayerModificationRequest;
import com.example.demo.app.player.service.PlayerService;

@RunWith(SpringRunner.class)
public class PlayerControllerTest {
	@MockBean
	private PlayerService playerService;

	private PlayerController toTest;
	private PlayerModificationRequest request;

	@Before
	public void setUp() {
		toTest = new PlayerController(playerService);
		request = PlayerModificationRequest.builder()
				.playerName("Rafal")
				.monthsOfExperience(33)
				.playerAge(27)
				.teamId(1L)
				.build();
	}

	@Test
	public void should_createPlayer_call_service_method_with_proper_argument() {
		//when
		toTest.createPlayer(request);
		//then
		BDDMockito.then(playerService).should().createPlayer(argThat(
				arg -> arg.getMonthsOfExperience() == request.getMonthsOfExperience() &&
						arg.getPlayerAge() == request.getPlayerAge() &&
						arg.getTeamId().equals(request.getTeamId()) &&
						arg.getPlayerName().equals(request.getPlayerName())
		));
	}

	@Test
	public void should_updatePlayer_call_service_method_with_proper_argument() {
		//when
		toTest.updatePlayer(request);
		//then
		BDDMockito.then(playerService).should().updatePlayer(argThat(
				arg -> arg.getMonthsOfExperience() == request.getMonthsOfExperience() &&
						arg.getPlayerAge() == request.getPlayerAge() &&
						arg.getTeamId().equals(request.getTeamId()) &&
						arg.getPlayerName().equals(request.getPlayerName())
		));
	}

	@Test
	public void should_deletePlayer_call_service_method_with_proper_argument() {
		//when
		toTest.deletePlayer(request.getPlayerId());
		//then
		BDDMockito.then(playerService).should().deletePlayer(request.getPlayerId());
	}

	@Test
	public void should_getPlayerFee_call_service_method_with_proper_argument() {
		//when
		toTest.getPlayerFee(request.getPlayerId());
		//then
		BDDMockito.then(playerService).should().getPlayerTransferFee(request.getPlayerId());
	}

	@Test
	public void should_getPlayers_call_service_method_with_proper_argument() {
		//when
		toTest.getPlayers();
		//then
		BDDMockito.then(playerService).should().getPlayers();
	}
}

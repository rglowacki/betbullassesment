package com.example.demo.app.player.service;

import static com.example.demo.app.player.data.PlayerDTO.toPlayerDTOLazy;
import static java.util.Arrays.asList;
import static org.assertj.core.api.BDDAssertions.then;
import static org.assertj.core.api.BDDAssertions.thenThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.app.player.data.PlayerDTO;
import com.example.demo.app.player.data.PlayerModificationRequest;
import com.example.demo.app.player.jpa.Player;
import com.example.demo.app.player.jpa.PlayerRepository;
import com.example.demo.app.team.data.TeamDTO;
import com.example.demo.app.team.jpa.Team;
import com.example.demo.app.team.jpa.TeamRepository;

@RunWith(SpringRunner.class)
public class PlayerServiceTest {

	@MockBean
	private PlayerRepository playerRepository;

	@MockBean
	private TeamRepository teamRepository;

	private PlayerService toTest;
	private PlayerModificationRequest request;
	private Team team;
	private Player player;

	@Before
	public void setUp() {
		toTest = new PlayerService(playerRepository, teamRepository);
		request = PlayerModificationRequest.builder()
				.playerId(1L)
				.playerName("Rafal")
				.monthsOfExperience(33)
				.playerAge(27)
				.teamId(1L)
				.build();
		team = Team.builder()
				.teamId(1L)
				.teamName("Raf Team")
				.teamCurrency("PLN").build();
		player = Player.builder()
				.playerId(1L)
				.team(team)
				.monthsOfExperience(33)
				.playerAge(27)
				.playerName("Rafal").build();
	}

	@Test
	public void should_createPlayer_create_new_player_with_team() {
		//given
		given(teamRepository.findByTeamId(request.getTeamId())).willReturn(Optional.of(team));
		given(playerRepository.save(any())).willReturn(player);
		//when
		PlayerDTO playerDTO = toTest.createPlayer(request);
		//then
		then(playerDTO.getMonthsOfExperience()).isEqualTo(request.getMonthsOfExperience());
		then(playerDTO.getPlayerAge()).isEqualTo(request.getPlayerAge());
		then(playerDTO.getPlayerName()).isEqualTo(request.getPlayerName());
		then(playerDTO.getTeam()).isEqualTo(TeamDTO.toTeamDTO(player.getTeam()));
	}

	@Test
	public void should_createPlayer_create_new_player_without_team() {
		//given
		Player playerWithoutTeam = Player.builder()
				.playerId(1L)
				.monthsOfExperience(33)
				.playerAge(27)
				.playerName("Rafal").build();
		given(teamRepository.findByTeamId(request.getTeamId())).willReturn(Optional.empty());
		given(playerRepository.save(any())).willReturn(playerWithoutTeam);
		//when
		PlayerDTO playerDTO = toTest.createPlayer(request);
		//then
		then(playerDTO.getMonthsOfExperience()).isEqualTo(request.getMonthsOfExperience());
		then(playerDTO.getPlayerAge()).isEqualTo(request.getPlayerAge());
		then(playerDTO.getPlayerName()).isEqualTo(request.getPlayerName());
		then(playerDTO.getTeam()).isEqualTo(null);
	}

	@Test
	public void should_updatePlayer_throw_exception_when_player_not_found() {
		//when
		thenThrownBy(() -> toTest.updatePlayer(request))
				//then
				.isInstanceOf(EntityNotFoundException.class)
				.hasMessage(String.format("Player with id %d not found", request.getPlayerId()));
	}

	@Test
	public void should_updatePlayer_with_new_team() {
		//given
		Team newTeam = Team.builder()
				.teamId(2L)
				.teamName("Glo FC")
				.teamCurrency("USD").build();
		PlayerModificationRequest updateRequest = PlayerModificationRequest.builder()
				.playerId(1L)
				.playerName("Martin")
				.monthsOfExperience(30)
				.playerAge(19)
				.teamId(2L)
				.build();
		Player updatedPlayer = Player.builder()
				.playerId(updateRequest.getPlayerId())
				.playerName(updateRequest.getPlayerName())
				.monthsOfExperience(updateRequest.getMonthsOfExperience())
				.playerAge(updateRequest.getPlayerAge())
				.team(newTeam).build();
		given(playerRepository.findByPlayerId(updateRequest.getPlayerId())).willReturn(Optional.of(player));
		given(teamRepository.findByTeamId(updateRequest.getTeamId())).willReturn(Optional.of(newTeam));
		given(playerRepository.save(updatedPlayer)).willReturn(updatedPlayer);
		//when
		PlayerDTO result = toTest.updatePlayer(updateRequest);
		//then
		then(result.getPlayerName()).isEqualTo(updatedPlayer.getPlayerName());
		then(result.getPlayerAge()).isEqualTo(updatedPlayer.getPlayerAge());
		then(result.getPlayerId()).isEqualTo(updatedPlayer.getPlayerId());
		then(result.getMonthsOfExperience()).isEqualTo(updatedPlayer.getMonthsOfExperience());
		then(result.getTeam()).isEqualTo(TeamDTO.toTeamDTO(updatedPlayer.getTeam()));
	}

	@Test
	public void should_updatePlayer_with_old_team() {
		//given
		PlayerModificationRequest updateRequest = PlayerModificationRequest.builder()
				.playerId(1L)
				.playerName("Martin")
				.monthsOfExperience(30)
				.playerAge(19)
				.build();
		Player updatedPlayer = Player.builder()
				.playerId(updateRequest.getPlayerId())
				.playerName(updateRequest.getPlayerName())
				.monthsOfExperience(updateRequest.getMonthsOfExperience())
				.playerAge(updateRequest.getPlayerAge())
				.team(team).build();
		given(playerRepository.findByPlayerId(updateRequest.getPlayerId())).willReturn(Optional.of(player));
		given(teamRepository.findByTeamId(updateRequest.getTeamId())).willReturn(Optional.of(team));
		given(playerRepository.save(updatedPlayer)).willReturn(updatedPlayer);
		//when
		PlayerDTO result = toTest.updatePlayer(updateRequest);
		//then
		then(result.getPlayerName()).isEqualTo(updatedPlayer.getPlayerName());
		then(result.getPlayerAge()).isEqualTo(updatedPlayer.getPlayerAge());
		then(result.getPlayerId()).isEqualTo(updatedPlayer.getPlayerId());
		then(result.getMonthsOfExperience()).isEqualTo(updatedPlayer.getMonthsOfExperience());
		then(result.getTeam()).isEqualTo(TeamDTO.toTeamDTO(updatedPlayer.getTeam()));
	}

	@Test
	public void should_deletePlayer_throw_exception_when_player_not_found() {
		//when
		thenThrownBy(() -> toTest.deletePlayer(request.getPlayerId()))
				//then
				.isInstanceOf(EntityNotFoundException.class)
				.hasMessage(String.format("Player with id %d not found", request.getPlayerId()));
	}

	@Test
	public void should_deletePlayer_success() {
		//given
		given(playerRepository.findByPlayerId(request.getPlayerId())).willReturn(Optional.of(player));
		//when
		toTest.deletePlayer(request.getPlayerId());
		//then
		BDDMockito.then(playerRepository).should().delete(player);
	}

	@Test
	public void should_getPlayerTransferFee_return_correct_value() {
		//given
		BigDecimal expectedValue = new BigDecimal("134444.44");
		given(playerRepository.findByPlayerId(request.getPlayerId())).willReturn(Optional.of(player));
		//when
		BigDecimal result = toTest.getPlayerTransferFee(request.getPlayerId());
		//then
		then(result).isEqualTo(expectedValue);
	}

	@Test
	public void should_getPlayerTransferFee_throw_exception_when_player_not_found() {
		//when
		thenThrownBy(() -> toTest.getPlayerTransferFee(request.getPlayerId()))
				//then
				.isInstanceOf(EntityNotFoundException.class)
				.hasMessage(String.format("Player with id %d not found", request.getPlayerId()));
	}

	@Test
	public void should_getPlayers_return_list() {
		//given
		 Player anotherPlayer = Player.builder()
				.playerId(2L)
				.playerAge(22)
				.monthsOfExperience(33)
				.playerName("Martin").build();
		List<Player> players = asList(player, anotherPlayer);
		given(playerRepository.findAll()).willReturn(players);
		//when
		List<PlayerDTO> resultList = toTest.getPlayers();
		//then
		then(resultList).containsOnly(toPlayerDTOLazy(player), toPlayerDTOLazy(anotherPlayer));
	}
}
